Zvini CLI
=========

Zvini CLI allows you to put and get files to and from a
[Zvini](https://github.com/zvini/website) instance using command line.

Installation
------------

Running `./install.sh` script as root will make `zvini-config`,
`zvini-file-get`, `zvini-file-list` and `zvini-file-put`
commands available on your system.

See Also
--------

* [Zvini Website](https://github.com/zvini/website)
