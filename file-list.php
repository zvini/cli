#!/usr/bin/php
<?php

include_once __DIR__.'/classes/ZviniClient.php';
$zviniClient = new ZviniClient;

$response = $zviniClient->call('folder/list', []);
if (gettype($response) == 'string') {
    include_once __DIR__.'/fns/error.php';
    if ($response == 'ACCESS_DENIED') error('Access denied.');
    else error($response);
}

foreach ($response as $folder) echo "$folder->name\n";

$response = $zviniClient->call('file/list', []);
if (gettype($response) == 'string') {
    include_once __DIR__.'/fns/error.php';
    if ($response == 'ACCESS_DENIED') error('Access denied.');
    else error($response);
}

foreach ($response as $file) echo "$file->name\n";
