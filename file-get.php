#!/usr/bin/php
<?php

function printUsage () {
    echo "Usage: zvini-file-get [-f] <remote_name> [<local_file>]\n"
        ." -f overwrite existing file\n";
    exit(1);
}

include_once __DIR__.'/classes/ZviniClient.php';
$zviniClient = new ZviniClient;

include_once __DIR__.'/lib/args.php';
if (!$args) printUsage();

$forceOverwrite = $args[0] == '-f';
if ($forceOverwrite) array_shift($args);

if (count($args) == 2) $local_file = $args[1];
elseif (count($args) != 1) printUsage();
else $local_file = null;

$name = $args[0];

$response = $zviniClient->call('file/list', []);
if (gettype($response) == 'string') {
    include_once __DIR__.'/fns/error.php';
    if ($response == 'ACCESS_DENIED') error('Access denied.');
    else error($response);
}

$found_file = null;
foreach ($response as $file) {
    if ($file->name === $name) {
        $found_file = $file;
        break;
    }
}

if ($found_file !== null) {
    include_once __DIR__.'/fns/get_file.php';
    get_file($zviniClient, $found_file, $local_file, $forceOverwrite);
    exit;
}

$response = $zviniClient->call('folder/list', []);
if (gettype($response) == 'string') {
    include_once __DIR__.'/fns/error.php';
    if ($response == 'ACCESS_DENIED') error('Access denied.');
    else error($response);
}

$found_folder = null;
foreach ($response as $folder) {
    if ($folder->name === $name) {
        $found_folder = $folder;
        break;
    }
}

if ($found_folder !== null) {
    include_once __DIR__.'/fns/get_folder.php';
    get_folder($zviniClient, $found_folder, $local_file, $forceOverwrite);
    exit;
}

include_once __DIR__.'/fns/error.php';
error(json_encode($name).' not found.');
