<?php

if (!isset($argv)) {
    include_once __DIR__.'/../fns/error.php';
    error("The command should be called from a terminal.");
}
$args = array_slice($argv, 1);
