#!/usr/bin/php
<?php

function printUsage () {
    echo "Usage: zvini-file-put [-f] <local_file> [<remote_name>]\n"
        ." -f overwrite existing file\n";
    exit(1);
}

include_once __DIR__.'/classes/ZviniClient.php';
$zviniClient = new ZviniClient;

include_once __DIR__.'/lib/args.php';
if (!$args) printUsage();

$forceOverwrite = $args[0] == '-f';
if ($forceOverwrite) array_shift($args);

if (count($args) == 2) $name = $args[1];
elseif (count($args) != 1) printUsage();
else $name = null;

include_once __DIR__.'/fns/put_folder_or_file.php';
put_folder_or_file($zviniClient, $args[0], $name, $forceOverwrite);
