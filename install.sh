#!/bin/bash
cd `dirname $BASH_SOURCE`
dir=`pwd`
ln -fs $dir/config.php /usr/bin/zvini-config
ln -fs $dir/file-get.php /usr/bin/zvini-file-get
ln -fs $dir/file-list.php /usr/bin/zvini-file-list
ln -fs $dir/file-put.php /usr/bin/zvini-file-put
