<?php

class ZviniClient {

    public $base;
    public $api_key;

    public $lastResponse;
    public $lastStatus;

    function __construct () {

        include_once __DIR__.'/../fns/get_config.php';
        get_config($zvini_location, $this->api_key);

        $this->base = "{$zvini_location}api-call/";

    }

    function call ($method, $params) {
        $this->exec($method, $params);
        return json_decode($this->lastResponse);
    }

    function download ($method, $params) {
        $this->exec($method, $params);
        return $this->lastStatus == 200;
    }

    function exec ($method, $params) {
        $params['api_key'] = $this->api_key;
        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_POSTFIELDS => $params,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_URL => "$this->base$method.php",
        ]);
        $this->lastResponse = curl_exec($ch);
        $this->lastStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    }

}
