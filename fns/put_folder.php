<?php

function put_folder ($zviniClient, $local_file,
    $name, $forceOverwrite, $parent_id = 0) {

    if ($name === null) {
        include_once __DIR__.'/str_collapse_spaces.php';
        $name = basename($local_file);
        $name = str_collapse_spaces($name);
    }

    if ($forceOverwrite) {

        $response = $zviniClient->call('folder/list', [
            'parent_id' => $parent_id,
        ]);
        if (gettype($response) == 'string') {
            include_once __DIR__.'/error.php';
            if ($response == 'ACCESS_DENIED') error('Access denied.');
            else error($response);
        }

        foreach ($response as $folder) {
            if ($folder->name === $name) {
                $response = $zviniClient->call('folder/delete', [
                    'id' => $folder->id,
                ]);
                if (gettype($response) == 'string') {
                    include_once __DIR__.'/error.php';
                    if ($response == 'ACCESS_DENIED') error('Access denied.');
                    else error($response);
                }
                break;
            }
        }

    }

    $response = $zviniClient->call('folder/add', [
        'name' => $name,
        'parent_id' => $parent_id,
    ]);
    if (gettype($response) == 'string') {
        include_once __DIR__.'/error.php';
        if ($response == 'ACCESS_DENIED') error('Access denied.');
        elseif ($response == 'FOLDER_ALREADY_EXISTS') {
            error(json_encode($name).' already exists.');
        } else {
            error($response);
        }
    }

    include_once __DIR__.'/put_folder_or_file.php';
    foreach (glob("$local_file/*") as $file) {
        put_folder_or_file($zviniClient, $file,
            null, $forceOverwrite, $response);
    }

}
