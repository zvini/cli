<?php

function get_config_file () {
    $user = posix_getpwuid(posix_getuid());
    return "$user[dir]/.zvini-cli";
}
