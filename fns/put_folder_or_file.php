<?php

function put_folder_or_file ($zviniClient,
    $local_file, $name, $forceOverwrite, $parent_id = 0) {

    if (is_dir($local_file)) {
        include_once __DIR__.'/put_folder.php';
        put_folder($zviniClient, $local_file,
            $name, $forceOverwrite, $parent_id);
    } elseif (is_file($local_file)) {
        include_once __DIR__.'/put_file.php';
        put_file($zviniClient, $local_file,
            $name, $forceOverwrite, $parent_id);
    } else {
        include_once __DIR__.'/error.php';
        error(json_encode($local_file).' is not a file.');
    }

}
