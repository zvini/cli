<?php

function get_folder ($zviniClient, $folder, $local_file, $forceOverwrite) {

    include_once __DIR__.'/safe_name.php';
    $safe_name = safe_name($folder->name);

    if ($local_file === null) {
        $local_file = $safe_name;
    } else {

        if (!is_dir(dirname($local_file))) {
            error(json_encode($local_file).' is not a directory.');
        }

        if (is_dir($local_file)) $local_file .= "/$safe_name";

    }

    if (is_dir($local_file) && !$forceOverwrite) {
        include_once __DIR__.'/error.php';
        error(json_encode($local_file).' already exists.');
    }

    mkdir($local_file);

    $params = ['parent_id' => $folder->id];

    $response = $zviniClient->call('file/list', $params);
    if (gettype($response) == 'string') {
        include_once __DIR__.'/error.php';
        if ($response == 'ACCESS_DENIED') error('Access denied.');
        else error($response);
    }

    include_once __DIR__.'/get_file.php';
    foreach ($response as $file) {
        get_file($zviniClient, $file, $local_file, $forceOverwrite);
    }

    $response = $zviniClient->call('folder/list', $params);
    if (gettype($response) == 'string') {
        include_once __DIR__.'/error.php';
        if ($response == 'ACCESS_DENIED') error('Access denied.');
        else error($response);
    }

    foreach ($response as $folder) {
        get_folder($zviniClient, $folder, $local_file, $forceOverwrite);
    }

}
