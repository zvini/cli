<?php

function get_file ($zviniClient, $file, $local_file, $forceOverwrite) {

    include_once __DIR__.'/safe_name.php';
    $safe_name = safe_name($file->name);

    if ($local_file === null) {
        $local_file = $safe_name;
    } else {

        if (!is_dir(dirname($local_file))) {
            error(json_encode($local_file).' is not a directory.');
        }

        if (is_dir($local_file)) $local_file .= "/$safe_name";

    }

    if (is_file($local_file) && !$forceOverwrite) {
        include_once __DIR__.'/error.php';
        error(json_encode($local_file).' already exists.');
    }

    $ok = $zviniClient->download('file/download', ['id' => $file->id]);
    if ($ok === false) {
        include_once __DIR__.'/error.php';
        error("Failed to download the file. HTTP status $zviniClient->lastStatus.");
    }

    file_put_contents($local_file, $zviniClient->lastResponse);

}
