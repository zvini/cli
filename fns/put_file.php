<?php

function put_file ($zviniClient, $local_file,
    $name, $forceOverwrite, $parent_id = 0) {

    if ($name === null) {
        include_once __DIR__.'/str_collapse_spaces.php';
        $name = basename($local_file);
        $name = str_collapse_spaces($name);
    }

    if ($forceOverwrite) {

        $response = $zviniClient->call('file/list', [
            'parent_id' => $parent_id,
        ]);
        if (gettype($response) == 'string') {
            include_once __DIR__.'/error.php';
            if ($response == 'ACCESS_DENIED') error('Access denied.');
            else error($response);
        }

        foreach ($response as $file) {
            if ($file->name === $name) {
                $response = $zviniClient->call('file/delete', [
                    'id' => $file->id,
                ]);
                if (gettype($response) == 'string') {
                    include_once __DIR__.'/error.php';
                    if ($response == 'ACCESS_DENIED') error('Access denied.');
                    else error($response);
                }
                break;
            }
        }

    }

    $response = $zviniClient->call('file/add', [
        'name' => $name,
        'file' => new CURLFile($local_file),
        'parent_id' => $parent_id,
    ]);
    if (gettype($response) == 'string') {
        include_once __DIR__.'/error.php';
        if ($response == 'ACCESS_DENIED') error('Access denied.');
        elseif ($response == 'FILE_ALREADY_EXISTS') {
            error(json_encode($name).' already exists.');
        } else {
            error($response);
        }
    }

}
