<?php

function safe_name ($name) {
    $name = str_replace('/', '_', $name);
    if ($name === '.') $name = '_';
    if ($name === '..') $name = '__';
    return $name;
}
