<?php

function get_config (&$zvini_location, &$api_key) {

    include_once __DIR__.'/get_config_file.php';
    $config_file = get_config_file();

    if (is_file($config_file) && is_readable($config_file)) {
        $content = @file_get_contents($config_file);
        if ($content !== false) {
            $document = json_decode($content);
            if (is_object($document) &&
                property_exists($document, 'zvini_location') &&
                property_exists($document, 'api_key')) {

                $zvini_location = $document->zvini_location;
                $api_key = $document->api_key;

                if (!is_string($zvini_location) || !is_string($api_key)) {
                    $zvini_location = $api_key = null;
                }

            }
        }
    }

}
