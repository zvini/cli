#!/usr/bin/php
<?php

include_once __DIR__.'/lib/args.php';
$num_args = count($args);
if ($num_args === 0) {

    include_once __DIR__.'/fns/get_config.php';
    get_config($zvini_location, $api_key);

    echo "zvini_location: $zvini_location\n"
        ."api_key: $api_key\n";

} elseif ($num_args === 2) {
    include_once __DIR__.'/fns/get_config_file.php';
    file_put_contents(get_config_file(), json_encode([
        'zvini_location' => $args[0],
        'api_key' => $args[1],
    ], JSON_PRETTY_PRINT)."\n");
} else {
    echo "Usages:\n"
        ." zvini-config\n"
        ." zvini-config <zvini_location> <api_key>\n";
    exit(1);
}
